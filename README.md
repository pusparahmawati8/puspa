In this program, it tries to extract Mel Frequency Cepstral Coefficients (MFCC) from audio with .wav format. This program uses the Python programming language.
The steps for making it are as follows:
1.	In the early stages (input speech signal) the audio with the name audio_sample.wav is uploaded to the content/sample_data folder on Google   
Drive (https://drive.google.com/file/d/1KQd69Ep2OXUL40r-uhzquWWRlXnNCUHd/view?usp=sharing). To be able to import the audio data, it is necessary to mount it from the drive to Colab. If you look at the script, there is fs which is a sampling frequency of 16kHz or equal to 16,000 Hz.
There are several libraries that are used such as numpy, librosa, os, matplotlib and several other libraries.
2.	In the representation of the signal, it is used to provide an overview of the signal from the input audio. In this stage, we use the matplotlib library with the plt or plot command to display the data in graphical form.
Viewed on the signal graph, it can be seen that there is a blue signal generated from audio_sample.wav. When audio_sample is played, there will be a pause or silence before the word no is heard. This makes the signal at the beginning of the audio flat. While the signal that has a high line, is a representation of the sound in the audio_sample which has the highest or clearest tone to be heard.
3.	in the third stage there is windowing which functions to create a pattern on the signal so that it can be processed for the next stage. This program uses Hamming type windowing so that it can be maximized in eliminating discontinuity effects. The duration of this windowing is 25 ms or can be written as 25e-3 or equal to 0.00025, with a shift of 10 ms or equal to 10e-3.
4.	In the next step is the Covariance matrix which aims to describe a measure of the strength of the correlation between two variables or more sets of variables. This comparison is obtained from the comparison at the windowing stage
5.	The fifth stage is Mel-Scale Transformation of Frequencies. The Mel Scale is a logarithmic transformation of a signal’s frequency. The core idea of this transformation is that sounds of equal distance on the Mel Scale are perceived to be of equal distance to humans.
6.	At this stage the conversion from linear linear scale to mel-scal and vice versa. So that the resulting comparison of frequencies in mels or Hz. In this conversion process using a different formula between the two frequency conversions.
7.	In signal processing, a filter bank (or filterbank) is an array of bandpass filters that separates the input signal into multiple components, each one carrying a single frequency sub-band of the original signal.
8.	In the Spectral Energy Weighting Filterbank section, there is a comparison between the uniform mel-bandwidth filterbank and the normalized area filter. There are two components or parameters being compared, namely weightening and frequency
9.	in the filterbank application, there is a comparison of the results of the linear frequency FFT bin with the filter index
10.	At the stage of using DCT, DCT functions to become an algorithm that can be used to compress signals or images. DCT then converts the data from spatial form to frequency form, then performs frequency data processing, and converts it into spatial form using the inversion of the relevant method.

REST API 
-	in making Rest API, using flask python, therefore it is necessary to install a virtual environment or commonly known as virtual env. After that proceed with installing flask. Flask is a library provided by Python specifically for software developers and website management.
-	Then in visual studio, two extensions were installed, namely flask-snippets and jinja2 snippets. When finished, you can call the flask library with the command from flask import Flask. 
-	After that create new files named routes.py, main.py, and run flask.
However, because there were some problems, I couldn't complete the task until the API was created according to the conditions. I've tried to make it, but it still fails and it doesn't work.
Sorry if the results are not in accordance with the existing provisions
